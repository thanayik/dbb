module.exports = {
    devServer: {
      proxy: 'http://localhost:5000'
    },
    // this is needed for a static build with
    // all paths set to be relative (and correct)
    // (e.g. bundling in an electron app, or serving the static
    // build from a server)
    publicPath: './',

    // see: https://bootstrap-vue.org/docs/reference/images
    // to understand why this chainWebpack section is needed
    chainWebpack: config => {
        config.module
          .rule('vue')
          .use('vue-loader')
          .loader('vue-loader')
          .tap(options => {
            options.transformAssetUrls = {
              img: 'src',
              image: 'xlink:href',
              'b-avatar': 'src',
              'b-img': 'src',
              'b-img-lazy': ['src', 'blank-src'],
              'b-card': 'img-src',
              'b-card-img': 'src',
              'b-card-img-lazy': ['src', 'blank-src'],
              'b-carousel-slide': 'img-src',
              'b-embed': 'src'
            }

            return options
          })
    },

    pages: {

      index: 'src/main.js',
      admin: 'src/admin.js',
    }
}
