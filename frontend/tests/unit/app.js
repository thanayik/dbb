import Navigation from "@/components/Navigation.vue";
import App from "@/App.vue";
import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";
import Vue from "vue"

// silence vue warnings about third party components (e.g. Bootstrapvue components included globally)
Vue.config.silent = true


describe("App", () => {
  let component;

  beforeEach(() => {
    component = shallowMount(App);
  });

  it("should render Navigation on mount", () => {
    expect(component.findComponent(Navigation).exists()).to.be.true;
  });

 
});