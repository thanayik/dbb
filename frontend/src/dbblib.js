// dbblib.js
// a library of helper functions for use in the digital brain bank web pages.
// Author: Taylor Hanayik
export var API_URL_NAMESPACE = "" //"/DigitalBrainBank"
export var API_DATASETS = API_URL_NAMESPACE + "/api/datasets" // <id>
export var API_TILES = API_URL_NAMESPACE + "/api/tiles"
export var API_PROJECTS =API_URL_NAMESPACE +  "/api/projects" //<kind>
export var API_THUMBNAILS = API_URL_NAMESPACE + "/api/thumbnails" // <id>
export var API_PUBLICATIONS = API_URL_NAMESPACE + "/api/publications" // <id>
export var API_STATIC_VIEWER_IMAGES = API_URL_NAMESPACE + "/api/static_viewer_images" //<id>
export var API_DOWNLOADS = API_URL_NAMESPACE + "/api/downloads" //<id>

export function getProjects(kind) {
  // kind: string -- one of "zoo", "anatomist", "pathologist"
  var response  = fetch(`${API_PROJECTS}/${kind}`)
  return response // returns a promise that must be resolved by the caller
}

export function getDataset(id) {
  var response  = fetch(`${API_DATASETS}/${id}`)
  return response // returns a promise that must be resolved by the caller
}

export function getThumbnail(id) {
  var response  = fetch(`${API_THUMBNAILS}/${id}`)
  return response // returns a promise that must be resolved by the caller
}

export function getStaticViewerImage(id) {
  var response  = fetch(`${API_STATIC_VIEWER_IMAGES}/${id}`)
  return response // returns a promise that must be resolved by the caller
}

export function getDownloads(id) {
  var response  = fetch(`${API_DOWNLOADS}/${id}`)
  return response // returns a promise that must be resolved by the caller
}

export function makeDownloadUrl(id, dlItem) {
  return `${API_DOWNLOADS}/${id}/${dlItem}`
}

export function makePublicationUrl(id) {
  return `${API_PUBLICATIONS}/${id}`
}

export function getPublication(id) {
  var response  = fetch(`${API_PUBLICATIONS}/${id}`)
  return response // returns a promise that must be resolved by the caller
}


export function getDownloadableFile(id, dlItem) {
  var response  = fetch(`${API_DOWNLOADS}/${id}/${dlItem}`)
  return response // returns a promise that must be resolved by the caller
}


export function makeThumbnailUrl(id) {
  return `${API_THUMBNAILS}/${id}`
}

export function makeStaticViewerUrl(id) {
  return `${API_STATIC_VIEWER_IMAGES}/${id}`
}

export function makeViewerUrl(id) {
  return `/tileviewer/${id}`
}

export function fileListFromString(fileStr) {
  // split a tile string by ":" and return an array of tile strings
  return fileStr.split(":")
}
