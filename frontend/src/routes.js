import Vue from "vue"
import Router from "vue-router"
import Home from "@/components/Home.vue"
import TileViewer from "@/components/TileViewer.vue"
import Contributors from "@/components/Contributors.vue"
import About from "@/components/About.vue"
//import PathologistData from "@/components/PathologistData.vue"
//import ZooData from "@/components/ZooData.vue"
//import AnatomistData from "@/components/AnatomistData.vue"
import DataPage from "@/components/DataPage.vue"
import InfoPage from "@/components/InfoPage.vue"

Vue.use(Router)

export default new Router (
  {
    routes: [
      {
        path: "/",
        name: "home",
        component: Home
      },
      {
        path: "/datasets/:kind",
        name: "datasets",
        component: DataPage
      },
      // {
      //   path: "/tileviewer/:dataid/:tiles/:min_zoom/:max_zoom",
      //   props: true,
      //   name: "tileviewer",
      //   component: TileViewer,
      // },
      {
        path: "/tileviewer/:id?",
        props: true,
        name: "tileviewerID",
        component: TileViewer,
      },
      // {
      //   path: "/tileviewer",
      //   name: "tileviewer",
      //   component: TileViewer,
      // },
      {
        path: "/contributors",
        name: "contributors",
        component: Contributors
      },
      {
        path: "/about",
        name: "about",
        component: About
      },
      {
        path: "/info/:id?",
        name: "info",
        component: InfoPage
      }

    ],
    mode: "hash"
  }
)
