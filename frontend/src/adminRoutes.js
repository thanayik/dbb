import Vue from "vue"
import Router from "vue-router"
import AdminHome from "@/components/AdminHome.vue"
import AdminLogin from "@/components/AdminLogin.vue"
import AdminViewDatasets from "@/components/AdminViewDatasets.vue"
import AdminNewDataset from "@/components/AdminNewDataset.vue"
import AdminEditDataset from "@/components/AdminEditDataset.vue"
import AdminNewContributor from "@/components/AdminNewContributor.vue"

Vue.use(Router)

export default new Router (
  {
    routes: [
      {
        path: "/",
        name: "admin",
        component:AdminHome
      },
      {
        path: "/adminlogin",
        name: "adminlogin",
        component: AdminLogin
      },
      {
        path: "/viewdatasets",
        name: "viewdatasets",
        component: AdminViewDatasets
      },
      {
        path: "/newdataset",
        name: "newdataset",
        component: AdminNewDataset
      },
      {
        path: "/newcontributor",
        name: "newcontributor",
        component: AdminNewContributor
      },
      {
        path: "/editdataset/:id",
        name: "editdataset",
        component: AdminEditDataset
      }
    ],
    mode: "hash"
  }
)
