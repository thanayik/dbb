# Digital Brain Bank

# Development Overview

## Frontend Development environment
```
cd frontend
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Installs into backend environment
```
npm run frontend:install
```

### Lints and fixes files
```
npm run lint
```

# Docker notes

docker-compose -f docker-compose-local.yml --env-file .env.local build
docker-compose -f docker-compose-local.yml --env-file .env.local up