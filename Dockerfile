FROM python:3.7.6-buster

LABEL AUTHOR="Taylor Hanayik"
LABEL AUTHOR_EMAIL="taylor.hanayik@ndcn.ox.ac.uk"
LABEL MAINTAINER="taylor.hanayik@ndcn.ox.ac.uk"

ARG container_gid
ARG user_uid

RUN groupadd -g ${container_gid} dbb
RUN useradd -u ${user_uid} -g ${container_gid} -ms /bin/bash dbb
WORKDIR /home/dbb

# COPY backend/requirements.txt requirements.txt
COPY backend app
RUN pip install -r app/requirements.txt

ENV FLASK_APP /home/dbb/app/dbbserver.py
RUN chown -R dbb:dbb ./
USER dbb

WORKDIR /home/dbb/app/
RUN chmod +x run.sh

ENTRYPOINT ["/home/dbb/app/run.sh"]

