import os
import json
import datetime
import shutil
import traceback
import time
import threading
from unicodedata import category
import uuid
from datetime import datetime, timedelta
from glob import glob
import sys

from flask import Flask, request, jsonify, send_from_directory, send_file, make_response, Response, stream_with_context, redirect, url_for
import requests
from werkzeug.utils import secure_filename
from flask_cors import CORS
from natsort import natsorted, humansorted
from passlib.hash import pbkdf2_sha256 as pwhash

import models


#############################################
#           App wide Helper functions
#############################################

def expand_path(p):
    '''expand user and vars'''
    if p is None:
        return p
    return os.path.expanduser(os.path.expandvars(p))

def list_tiles(layer_dir):
    '''
    return all top level tile dirs found in a layer dir

    tile dirs must be named <name>.tiles
    '''
    ldir = expand_path(layer_dir)
    tiles = glob(
        os.path.join(
            layer_dir,
            '*.tiles'
            )
        )
    return natsorted(tiles)


def zooms_from_tiles(tile_dir):
    '''
    returns a tuple of (min, max) zoom levels for a given tile folder
    '''
    zooms = next(os.walk(tile_dir))[1]
    zooms_sorted = natsorted(zooms)
    return (int(zooms_sorted[0]), int(zooms_sorted[-1]))


def last_in_path(path_str):
    '''
    return the last item in a path string (the string to the right of the last path separator)
    '''
    return path_str.split(sep=os.path.sep)[-1]



##############################################
#          FLASK APP CONFIG
##############################################

DBB_DATA_PROTECTED = expand_path(os.environ.get("DBB_DATA_PROTECTED"))
DBB_DATA_PUBLIC = expand_path(os.environ.get("DBB_DATA_PUBLIC"))
app = Flask(__name__, static_url_path='', static_folder='static')
CORS(app)
app.config['UPLOAD_PROTECTED'] = DBB_DATA_PROTECTED
app.config['UPLOAD_PUBLIC'] = DBB_DATA_PUBLIC
app.config['APPLICATION_ROOT'] = '/DigitalBrainBank'


###############################################
#          route and db convenience functions
##############################################

def extract_archive(archive):
    '''
    given an archive, extract it save it to UPLOAD_PUBLIC
    '''
    dir_name = os.path.splitext(os.path.basename(archive))[0]
    target_dir = os.path.join(app.config['UPLOAD_PUBLIC'], dir_name)
    # os.makedirs(target_dir, exist_ok=True)
    # tar -czf macaque-bigmac-92.layers.tar.gz * # do not tar layers, but rather the folders contained within the layers dir
    shutil.unpack_archive(archive, target_dir)
    return dir_name


def is_archive(file):
    '''
    returns true if file ends in .tar.gz or .zip
    '''
    if file.endswith('.tar.gz'):
        return True
    elif file.endswith('.zip'):
        return True
    return False

def uuid_str():
    '''
    generate a uuidv4 string
    '''
    return str(uuid.uuid4())
    

def is_valid_contrib_token(token):
    '''
    verify that token is indeed from an authorized contributor
    '''
    #TODO convert token to uuid string or JWT
    contributor = models.Contributor.objects(authToken=token).first()
    if contributor:
        now = datetime.utcnow()
        if contributor.authTokenExpireDate > now:
            return True
    return False

def is_valid_user_token(token):
    '''
    verify that token is indeed from an authorized user with download rights
    '''
    user = models.User.objects(authToken=token).first()
    if user:
        now = datetime.utcnow()
        if user.authTokenExpireDate > now:
            return True
    return False

def update_auth_token(contributor):
    '''
    generate a new auth token and update the expiration date
    '''
    new_token = pwhash.hash(contributor.passwordHash)
    new_expiration = datetime.utcnow() + timedelta(days=1)
    contributor.authToken = new_token
    contributor.authTokenExpireDate = new_expiration
    contributor.save()
    contributor.reload()
    return contributor
    
@app.route('/', methods=['GET'])
def index():
    admin_contributor = models.Contributor.objects(username=os.environ.get("DBB_ADMIN_USERNAME")).first()
    if admin_contributor is None:
        print('creating admin user', flush=True)
        admin_passwordHash = pwhash.hash(os.environ.get("DBB_ADMIN_PASSWORD"))
        admin_authToken = pwhash.hash(admin_passwordHash)
        admin_authTokenExpireDate = datetime.utcnow() + timedelta(days=1)
        admin_contributor = models.Contributor(
            username=os.environ.get("DBB_ADMIN_USERNAME"),
            email=os.environ.get("DBB_ADMIN_EMAIL"),
            name='admin',
            role='admin',
            passwordHash=admin_passwordHash,
            authToken=admin_authToken,
            authTokenExpireDate=admin_authTokenExpireDate)
        admin_contributor.save()
    return app.send_static_file('index.html')

@app.route('/upload_protected_file', methods=['GET', 'POST'])
def upload_protected():
    if request.method == 'POST':
        token = request.args.get('token')
        if not token or not is_valid_contrib_token(token):
            return jsonify({'error': 'token not given or invalid'})
        if 'files' not in request.files:
            return jsonify({'error': 'no files'})
        
        files = request.files.getlist('files')
        saved_filelist = []
        for file in files:
            if file.filename == '':
                return jsonify({'error': 'no file'})
            if file:
                given_name = secure_filename(file.filename)
                filename = uuid_str() + '_' + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_PROTECTED'], filename))
                db_downloadable_file = models.DownloadableFile(
                    name=given_name,
                    uid=filename
                )
                db_downloadable_file.save()
                saved_filelist.append(
                    json.loads(db_downloadable_file.to_json())
                    )
        
        return jsonify({'success': saved_filelist})
    return jsonify({'error': 'GET request not supported'})

@app.route('/upload_public_file', methods=['GET', 'POST'])
def upload_public():
    if request.method == 'POST':
        token = request.args.get('token')
        if not token or not is_valid_contrib_token(token):
            return jsonify({'error': 'token not given or invalid'})
        if 'files' not in request.files:
            return jsonify({'error': 'no files'})
        
        files = request.files.getlist('files')
        saved_filelist = []
        for file in files:
            if file.filename == '':
                return jsonify({'error': 'no file'})
            if file:
                given_name = secure_filename(file.filename)
                filename = uuid_str() + '_' + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_PUBLIC'], filename))
                print(app.config['UPLOAD_PUBLIC'], flush=True)
                print(filename, flush=True)
                db_public_file = models.PublicFile(
                    name=given_name,
                    uid=filename
                )
                db_public_file.save()
                saved_filelist.append(
                    json.loads(db_public_file.to_json())
                    )
        
        return jsonify({'success': saved_filelist})
    return jsonify({'error': 'GET request not supported'})

@app.route('/upload_tiles', methods=['GET', 'POST'])
def upload_tiles():
    '''
    upload a tar.gz archive of viewer tiles and return the db entry
    '''
    if request.method == 'POST':
        token = request.args.get('token')
        if not token or not is_valid_contrib_token(token):
            return jsonify({'error': 'token not given or invalid'})
        if 'files' not in request.files:
            return jsonify({'error': 'no files'})
        
        files = request.files.getlist('files')
        saved_filelist = []
        for file in files:
            if file.filename == '':
                return jsonify({'error': 'no file'})
            if file:
                display_name = request.form.get('displayName', secure_filename(file.filename))
                filename = uuid_str() + '_' + secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_PUBLIC'], filename))
                if is_archive(filename):
                    filename = extract_archive(os.path.join(app.config['UPLOAD_PUBLIC'], filename))
                db_viewerTile_file = models.ViewerTile(
                    displayName=display_name,
                    uid=filename,
                    isBaseLayer=request.form.get('isBaseLayer', False)
                )
                db_viewerTile_file.save()
                saved_filelist.append(
                    json.loads(db_viewerTile_file.to_json())
                    )
        
        return jsonify({'success': saved_filelist})
    return jsonify({'error': 'GET request not supported'})


@app.route('/viewer_tiles/<id>/<z>/<y>/<x>', methods=['GET'])
def get_viewer_tiles(id, z, y, x):
    '''
    return the tile requested by leaflet
    '''
    try:
        tile = models.ViewerTile.objects(pk=id).first()
        return send_file(
            os.path.join(
                app.config['UPLOAD_PUBLIC'],
                tile.uid,
                "{}".format(z),
                "{}".format(y),
                "{}.jpg".format(x)
            )
        )
    except:
        return 'tile does not exist'

@app.route('/public_file/<file>', methods=['GET'])
def get_public(file):
    try:
        return send_from_directory(app.config['UPLOAD_PUBLIC'], path=file)
    # except:
        # return 'file does not exist: {}'.format(file)
    except Exception as e:
        traceback.print_exc()
        return jsonify({'error': '{}'.format(e)})

@app.route('/thumbnails/<id>', methods=['GET'])
def get_thumbnail(id):
    try:
        thumbnail = models.PublicFile.objects(pk=id).first()
        file = thumbnail.uid
        print(file)
        return send_from_directory(app.config['UPLOAD_PUBLIC'], path=file)
    except:
        print('got here')
        return 'thumbnail reference does not exist: {}'.format(id)

@app.route('/protected_file/<file>', methods=['GET'])
def get_protected(file):
    token = request.args.get('token')
    can_download = True
    # if token:
    #     can_download = is_valid_user_token(token)
    #     if not can_download:
    #         can_download = is_valid_contrib_token(token)
    if not can_download:
        return jsonify({'error': 'token not given or invalid'})
    try:
        return send_from_directory(app.config['UPLOAD_PROTECTED'], path=file)
    except:
        return 'file does not exist: {}'.format(file)

@app.route('/upload_dataset', methods=['GET', 'POST'])
def upload_dataset():
    token = request.args.get('token')
    if not token or not is_valid_contrib_token(token):
        return jsonify({'error': 'token not given or invalid'})
    data = request.json
    try:
        dataset_existing_datasetID = data.get('id')
        dataset_category = data.get('category')
        dataset_datasetTitle = data.get('datasetTitle')
        dataset_contentListEntries = data.get('datasetContentList')
        dataset_contentList = []
        for contentList_entry in dataset_contentListEntries:
            dataset_contentList.append(
                models.DatasetContent(title=contentList_entry['title'], content=contentList_entry['content'])
            )
        dataset_datasetContentFooter = data.get('datasetContentFooter')

        thumbnail_entry = data.get('thumbnail')
        if thumbnail_entry:
            print(thumbnail_entry, flush=True)
            dataset_thumbnail = models.PublicFile.objects(
                uid=thumbnail_entry.get('uid')
            ).first()

        dataset_viewerTextDescription = data.get('viewerTextDescription')
        dataset_downloadableFilesEntries = data.get('downloadableFiles')
        dataset_downloadableFiles = []
        for downloadableFile_entry in dataset_downloadableFilesEntries:
            dataset_downloadableFiles.append(
                models.DownloadableFile.objects(
                    uid=downloadableFile_entry.get('uid')
                ).first()
            )
        dataset_previewFilesEntries = data.get('previewFiles')
        dataset_previewFiles = []
        for previewFile_entry in dataset_previewFilesEntries:
            dataset_previewFiles.append(
                models.PublicFile.objects(
                    uid=previewFile_entry.get('uid')
                ).first()
            )
        dataset_viewerTilesEntries = data.get('viewerTiles')
        dataset_viewerTiles = []
        for viewerTile_entry in dataset_viewerTilesEntries:
            tile = models.ViewerTile.objects(uid=viewerTile_entry.get('uid')).first() 
            tile.isBaseLayer = viewerTile_entry.get('isBaseLayer', False)
            tile.displayName = viewerTile_entry.get('displayName', tile.displayName)
            tile.save()
            tile.reload()
            dataset_viewerTiles.append(tile)
        # viewer_static_image_entry = data.get('viewerStaticImages')
        # if viewer_static_image_entry:
        #     dataset_viewerStaticImage = models.PublicFile.objects(
        #         uid=viewer_static_image_entry[0].get('uid')
        #     ).first()
        # else:
        #     dataset_viewerStaticImage = {}
        dataset_viewerStaticImageEntries = data.get('viewerStaticImages')
        dataset_viewerStaticImages = []
        for dataset_viewerStaticImage in dataset_viewerStaticImageEntries:
            dataset_viewerStaticImages.append(
                models.PublicFile.objects(
                    uid=dataset_viewerStaticImage.get('uid')
                ).first()
            )
        dataset_requiresMTA = data.get('requiresMTA')
        dataset_isVisible = data.get('isVisible', True)
        dataset_correspondingAuthorEmail = data.get('correspondingAuthorEmail')
        dataset_projectTitle = data.get('projectTitle')
        dataset_projectDescription = data.get('projectDescription')
        # for simplicity, just use the admin contributor submitting this dataset for now
        dataset_adminContributors = [
            models.Contributor.objects(authToken=token).first()
        ]
        dataset_allowedUsersEntries = data.get('allowedUsers')
        dataset_allowedUsers = []
        print(dataset_allowedUsersEntries)
        for allowedUser_entry in dataset_allowedUsersEntries:
            dataset_allowedUsers.append(
                models.User.objects(
                    username=allowedUser_entry.get('username')
                ).first()
            )
        dataset_associatedPublications = data.get('associatedPublications')
        dataset_mainContributors = data.get('mainContributors')
        dataset_additionalContributors = data.get('additionalContributors')
        dataset_acknowledgements = data.get('acknowledgements')
        dataset_citationInstructions = data.get('citationInstructions')
        dataset_createdAt = datetime.utcnow()
        dataset_modifiedAt = datetime.utcnow()

        if dataset_existing_datasetID:
            dataset = models.Dataset.objects(pk=dataset_existing_datasetID).first()
            dataset.category=dataset_category
            dataset.datasetTitle=dataset_datasetTitle
            dataset.datasetContentList=dataset_contentList
            dataset.datasetContentFooter=dataset_datasetContentFooter
            dataset.thumbnail=dataset_thumbnail
            dataset.viewerTextDescription=dataset_viewerTextDescription
            dataset.downloadableFiles=dataset_downloadableFiles
            dataset.previewFiles=dataset_previewFiles
            dataset.viewerTiles=dataset_viewerTiles
            dataset.viewerStaticImages=dataset_viewerStaticImages
            dataset.requiresMTA=dataset_requiresMTA
            dataset.correspondingAuthorEmail=dataset_correspondingAuthorEmail
            dataset.projectTitle=dataset_projectTitle
            dataset.projectDescription=dataset_projectDescription
            dataset.adminContributors=dataset_adminContributors
            dataset.allowedUsers=dataset_allowedUsers
            dataset.associatedPublications=dataset_associatedPublications
            dataset.acknowledgements=dataset_acknowledgements
            dataset.mainContributors=dataset_mainContributors
            dataset.additionalContributors=dataset_additionalContributors
            dataset.citationInstructions=dataset_citationInstructions
            dataset.lastmodifiedAt=dataset_modifiedAt
            dataset.isVisible = dataset_isVisible
        else:
            # now create a new dataset object and save it to the database
            dataset = models.Dataset(
                category=dataset_category,
                datasetTitle=dataset_datasetTitle,
                datasetContentList=dataset_contentList,
                datasetContentFooter=dataset_datasetContentFooter,
                thumbnail=dataset_thumbnail,
                viewerTextDescription=dataset_viewerTextDescription,
                downloadableFiles=dataset_downloadableFiles,
                previewFiles=dataset_previewFiles,
                viewerTiles=dataset_viewerTiles,
                viewerStaticImages=dataset_viewerStaticImages,
                requiresMTA=dataset_requiresMTA,
                correspondingAuthorEmail=dataset_correspondingAuthorEmail,
                projectTitle=dataset_projectTitle,
                projectDescription=dataset_projectDescription,
                adminContributors=dataset_adminContributors,
                allowedUsers=dataset_allowedUsers,
                associatedPublications=dataset_associatedPublications,
                acknowledgements=dataset_acknowledgements,
                mainContributors=dataset_mainContributors,
                additionalContributors=dataset_additionalContributors,
                citationInstructions=dataset_citationInstructions, 
                createdAt=dataset_createdAt,
                lastmodifiedAt=dataset_modifiedAt,
                isVisible=dataset_isVisible
            )
        
        dataset.save()
        return jsonify({'success': dataset.to_dict()})
    except Exception as e:
        traceback.print_exc()
        return jsonify({'error': '{}'.format(e)})


@app.route('/contributor_login', methods=['GET', 'POST'])
def contributor_login():
    '''
    login to the api and return an auth token with expiration datetime
    '''
    username = request.json.get('username')
    password = request.json.get('password')

    contributor = models.Contributor.objects(username=username).first()
    if contributor:
        if pwhash.verify(password, contributor.passwordHash):
            contributor = update_auth_token(contributor)
            return jsonify({'success': json.loads(contributor.to_json())})
    return jsonify({'error': 'contributor does not exist, or invalid login'})

@app.route('/verify_token', methods=['GET', 'POST'])
def verify_token():
    '''
    verify that a supplied token is valid
    '''
    contributorToken = request.json.get('contributorToken')
    if contributorToken is not None:
        if is_valid_contrib_token(contributorToken):
            contributor = models.Contributor.objects(authToken=contributorToken).first()
            return jsonify({'success': json.loads(contributor.to_json())})
        else:
            jsonify({'error': 'invalid token'})
    return jsonify({'error': 'invalid token'})

@app.route('/contributor_register', methods=['GET', 'POST'])
def contributor_register():
    '''
    register a new contributor. Must be done by an existing contributor with role='admin'
    '''
    token = request.args.get('token')
    if not token or not is_valid_contrib_token(token):
        return jsonify({'error': 'token not given or invalid'})
    username = request.json.get('username')
    password = request.json.get('password')
    name = request.json.get('name')
    role = request.json.get('role', 'contributor')
    email = request.json.get('email')

    if not username and not password and not email and not name:
        return jsonify({'error': 'name or username or password or email is null'})

    contributor = models.Contributor.objects(username=username).first()
    if contributor:
        return jsonify({'error': 'username already exists'})
    else:
        passwordHash = pwhash.hash(password)
        authToken = pwhash.hash(passwordHash)
        authTokenExpireDate = datetime.utcnow() + timedelta(days=1)
        contributor = models.Contributor(
            username=username,
            email=email,
            name=name,
            role=role,
            passwordHash=passwordHash,
            authToken=authToken,
            authTokenExpireDate=authTokenExpireDate)
        contributor.save()
    return jsonify({'success': json.loads(contributor.to_json())})


@app.route('/user_register', methods=['GET', 'POST'])
def user_register():
    '''
    register a new user. Must be done by an existing contributor with role='admin'
    '''
    token = request.args.get('token')
    if not token or not is_valid_contrib_token(token):
        return jsonify({'error': 'token not given or invalid'})
    username = request.json.get('username')
    password = request.json.get('password')
    name = request.json.get('name')
    email = request.json.get('email')

    if not username and not password and not email and not name:
        return jsonify({'error': 'name or username or password or email is null'})

    user = models.User.objects(username=username).first()
    if user:
        return jsonify({'error': 'username already exists'})
    else:
        passwordHash = pwhash.hash(password)
        authToken = pwhash.hash(passwordHash)
        authTokenExpireDate = datetime.utcnow() + timedelta(days=1)
        user = models.User(
            username=username,
            email=email,
            name=name,
            passwordHash=passwordHash,
            authToken=authToken,
            authTokenExpireDate=authTokenExpireDate)
        user.save()
        
    return jsonify({'success': json.loads(user.to_json())})


@app.route('/datasets', methods=['GET'])
def get_datasets():
    '''
    return a list of datasets

    subcategories of datasets can be requests by using "category": "zoo" | "pathologist" | "anatomist" in the query string
    '''
    id = request.args.get('id')
    if id is not None:
        datasets = models.Dataset.objects(pk=id).first()
        return jsonify({'success': datasets.to_dict()})
    
    datasetTitle = request.args.get('datasetTitle')
    if datasetTitle is not None:
        datasets = models.Dataset.objects(datasetTitle=datasetTitle).first()
        return jsonify({'success': datasets.to_dict()})

    category = request.args.get('category')
    if category is not None:
        datasets = models.Dataset.objects(category=category)
    else:
        datasets = models.Dataset.objects
    return jsonify({'success': [dataset.to_dict() for dataset in datasets]})


if __name__ == "__main__":
    # gunicorn -b 127.0.0.1:8888 --log-level debug --timeout 3600 --workers 2 dbbserver:app
    app.run(host='127.0.0.1', port=8888, debug=True)

