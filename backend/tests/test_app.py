import json
from math import e
import os
import tempfile
import pytest
import json
import dbbserver
import models
from werkzeug.datastructures import FileStorage

TEST_CONTRIBUTOR = {
        'username': 'test_contributor',
        'password': 'test_password',
        'email': 'test_email@email.com',
        'name': 'jane doe'
}

TEST_USER = {
        'username': 'test_user',
        'password': 'test_user_password',
        'email': 'test_user_email@email.com',
        'name': 'john doe'
}

DBB_ADMIN = {
        'username': os.environ.get("DBB_ADMIN_USERNAME"),
        'password': os.environ.get("DBB_ADMIN_PASSWORD"),
        'email': os.environ.get("DBB_ADMIN_EMAIL"),
        'name': 'admin'
}

pytest.TEST_DATASET_ID = ''
pytest.TEST_TILES_ID = ''

@pytest.fixture
def client():
    dbbserver.app.config['TESTING'] = True
    DBB_DATA_PROTECTED = dbbserver.expand_path(os.environ.get("DBB_DATA_PROTECTED"))
    DBB_DATA_PUBLIC = dbbserver.expand_path(os.environ.get("DBB_DATA_PUBLIC"))
    dbbserver.app.config['UPLOAD_PROTECTED'] = DBB_DATA_PROTECTED
    dbbserver.app.config['UPLOAD_PUBLIC'] = DBB_DATA_PUBLIC

    with dbbserver.app.test_client() as client:
        yield client


def test_register_new_contributor(client):
    '''
    create a new contributor and add it to the database
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': DBB_ADMIN.get('username'),
                'password': DBB_ADMIN.get('password'),
                'name': DBB_ADMIN.get('name')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    admin = res_contrib_json.get('success')
    # admin = json.loads(admin)
    token = admin.get('authToken')

    res = client.post('/contributor_register',
        data=json.dumps(TEST_CONTRIBUTOR),
        content_type='application/json',
        query_string={'token': token}
    )
    res_json = json.loads(res.data)
    assert res_json.get('success') is not None


def test_register_new_user(client):
    '''
    create a new user and add it to the database
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': DBB_ADMIN.get('username'),
                'password': DBB_ADMIN.get('password'),
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    admin = res_contrib_json.get('success')
    # admin = json.loads(admin)
    token = admin.get('authToken')

    res = client.post('/user_register',
        data=json.dumps(TEST_USER),
        content_type='application/json',
        query_string={'token': token}
    )
    res_json = json.loads(res.data)
    assert res_json.get('success') is not None

def test_contributor_login(client):
    '''
    test that a contributor can login with a username and password
    '''
    res = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_json = json.loads(res.data)
    assert res_json.get('success') is not None

def test_invalid_contributor_password(client):
    '''
    test that an invalid login is correctly rejected
    '''
    res = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': 'wrong_password'
            }
        ),
        content_type='application/json'
    )
    res_json = json.loads(res.data)
    assert res_json.get('error') is not None

def test_invalid_contributor_username(client):
    '''
    test that an invalid login is correctly rejected
    '''
    res = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': 'wrong_user',
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_json = json.loads(res.data)
    assert res_json.get('error') is not None


def test_upload_protected_file(client):
    '''
    test that we can upload a protected file to the server
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    contributor = res_contrib_json.get('success')
    # contributor = json.loads(contributor)
    token = contributor.get('authToken')
    print(token)

    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file.nii.gz',
        ),
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file2.nii.gz',
        ),
    ]
    res = client.post('/upload_protected_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_json = json.loads(res.get_data(as_text=True))
    assert res_json.get('success') is not None

def test_upload_public_file(client):
    '''
    test that we can upload a public file to the server
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    contributor = res_contrib_json.get('success')
    # contributor = json.loads(contributor)
    token = contributor.get('authToken')

    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file.nii.gz',
        ),
    ]
    res = client.post('/upload_public_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_json = json.loads(res.get_data(as_text=True))
    assert res_json.get('success') is not None


def test_upload_tiles(client):
    '''
    test that we can upload a tar.gz archive of tiles
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    contributor = res_contrib_json.get('success')
    # contributor = json.loads(contributor)
    token = contributor.get('authToken')

    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_tiles.tar.gz'), 'rb'),
            filename='sample_tiles.tar.gz',
        )
    ]
    res = client.post('/upload_tiles',
        data={'files': files_to_upload, 'displayName': 'sample_tiles'},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_json = json.loads(res.get_data(as_text=True))
    assert res_json.get('success') is not None
    print(res_json)
    pytest.TEST_TILES_ID = res_json.get('success')[0].get('_id').get('$oid')


def test_is_valid_contrib_token():
    '''
    test if the contributor has a valid auth token
    '''
    contributor = models.Contributor.objects(username=TEST_CONTRIBUTOR.get('username')).first()
    assert dbbserver.is_valid_contrib_token(contributor.authToken) is True

def test_is_valid_user_token():
    '''
    test if the user has a valid auth token
    '''
    user = models.User.objects(username=TEST_USER.get('username')).first()
    assert dbbserver.is_valid_user_token(user.authToken) is True

def test_update_auth_token():
    '''
    test that we can update the auth token for a contributor
    '''
    contributor = models.Contributor.objects(username=TEST_CONTRIBUTOR.get('username')).first()
    old_token = contributor.authToken
    new_token = dbbserver.update_auth_token(contributor).authToken
    assert old_token != new_token

def test_is_archive():
    '''
    test if a filename is a valid archive we accept and know we can extract. Mostly used for viewer tiles
    '''
    assert dbbserver.is_archive('/some/path/myfile.tar.gz') is True
    assert dbbserver.is_archive('/some/path/myfile.zip') is True
    assert dbbserver.is_archive('/some/path with spaces/myfile.zip') is True
    assert dbbserver.is_archive('/some/path/myfile.nii.gz') is False

def test_upload_dataset(client):
    '''
    test that we can upload a new dataset via json
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    contributor = res_contrib_json.get('success')
    # contributor = json.loads(contributor)
    token = contributor.get('authToken')

    ## upload thumbnail
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/thumbnail.png'), 'rb'),
            filename='thumbnail.png',
        ),
    ]
    res_thumbnail = client.post('/upload_public_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_thumbnail_list = json.loads(res_thumbnail.data).get('success')

    ## upload downloadable files
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file.nii.gz',
        ),
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file2.nii.gz',
        ),
    ]
    res_downloadable_files = client.post('/upload_protected_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_downloadable_files_list = json.loads(res_downloadable_files.data).get('success')

    ## upload preview files
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file.nii.gz',
        ),
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file2.nii.gz',
        ),
    ]
    res_preview_files = client.post('/upload_public_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_preview_files_list = json.loads(res_preview_files.data).get('success')

    ## upload tiles
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_tiles.tar.gz'), 'rb'),
            filename='sample_tiles.tar.gz',
        )
    ]
    res_tiles = client.post('/upload_tiles',
        data={'files': files_to_upload, 'displayName': 'sample_tiles'},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_viewer_tiles_list = json.loads(res_tiles.data).get('success')

    dataset = {
        'category': 'zoo',
        'datasetTitle': 'first title',
        'datasetContent': [
            {
                'title': 'sample content title',
                'content':['one', 'two', 'three']
            }
        ],
        'datasetContentFooter': 'content footer text',
        'thumbnail': res_thumbnail_list,
        'viewerTextDescription': 'some long string',
        'downloadableFiles': res_downloadable_files_list,
        'previewFiles': res_preview_files_list,
        'viewerTiles': res_viewer_tiles_list,
        'viewerStaticImages': '',
        'requiresMTA': True,
        'correspondingAuthorEmail': 'test@email.com',
        'projectTitle': 'project1',
        'projectDescription': 'some long description',
        'allowedUsers': [
            TEST_USER
        ],
        'associatedPublications': [
            'pub1', 'pub2'
        ],
        'mainContributors': [
            'contribA', 'contribB'
        ],
        'additionalContributors': [
            'additionalA', 'additionalB'
        ],
        'acknowledgements': 'some acknowledgement',
        'citationInstructions': [
            'citeMeA', 'citeMeB'
        ],
    }

    res = client.post('/upload_dataset',
        data=json.dumps(dataset),
        content_type='application/json',
        query_string={'token': token}
    )
    res_dict = json.loads(res.data)

    # try a second dataset (but same info) so that we can test the dataset query web UI
    dataset['category'] = 'pathologist'
    res2 = client.post('/upload_dataset',
        data=json.dumps(dataset),
        content_type='application/json',
        query_string={'token': token}
    )
    assert res_dict.get('success') is not None 
    pytest.TEST_DATASET_ID = res_dict.get('success').get('_id').get('$oid')


def test_update_dataset(client):
    '''
    test that we can update a dataset via json
    '''
    res_contrib = client.post('/contributor_login',
        data=json.dumps(
            {
                'username': TEST_CONTRIBUTOR.get('username'),
                'password': TEST_CONTRIBUTOR.get('password')
            }
        ),
        content_type='application/json'
    )
    res_contrib_json = json.loads(res_contrib.data)
    contributor = res_contrib_json.get('success')
    # contributor = json.loads(contributor)
    token = contributor.get('authToken')

    ## upload thumbnail
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/thumbnail.png'), 'rb'),
            filename='thumbnail.png',
        ),
    ]
    res_thumbnail = client.post('/upload_public_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_thumbnail_list = json.loads(res_thumbnail.data).get('success')

    ## upload downloadable files
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file3.nii.gz',
        ),
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file4.nii.gz',
        ),
    ]
    res_downloadable_files = client.post('/upload_protected_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_downloadable_files_list = json.loads(res_downloadable_files.data).get('success')

    ## upload preview files
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file.nii.gz',
        ),
        FileStorage(
            stream=open(os.path.abspath('tests/sample_file.nii.gz'), 'rb'),
            filename='sample_file2.nii.gz',
        ),
    ]
    res_preview_files = client.post('/upload_public_file',
        data={'files': files_to_upload},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_preview_files_list = json.loads(res_preview_files.data).get('success')

    ## upload tiles
    files_to_upload = [
        FileStorage(
            stream=open(os.path.abspath('tests/sample_tiles.tar.gz'), 'rb'),
            filename='sample_tiles.tar.gz',
        )
    ]
    res_tiles = client.post('/upload_tiles',
        data={'files': files_to_upload, 'displayName': 'sample_tiles'},
        content_type='multipart/form-data',
        query_string={'token': token}
    )
    res_viewer_tiles_list = json.loads(res_tiles.data).get('success')

    dataset = {
        'category': 'zoo',
        'datasetTitle': 'test_dataset_updated',
        'datasetContent': [
            {
                'title': 'sample content title',
                'content':['one', 'two', 'three']
            }
        ],
        'datasetContentFooter': 'content footer text',
        'thumbnail': res_thumbnail_list,
        'viewerTextDescription': 'some long string',
        'downloadableFiles': res_downloadable_files_list,
        'previewFiles': res_preview_files_list,
        'viewerTiles': res_viewer_tiles_list,
        'viewerStaticImages': [],
        'requiresMTA': True,
        'correspondingAuthorEmail': 'test@email.com',
        'projectTitle': 'project1',
        'projectDescription': 'some long description',
        'allowedUsers': [
            TEST_USER
        ],
        'associatedPublications': [
            'pub1', 'pub2'
        ],
        'mainContributors': [
            'contribA'
        ],
        'additionalContributors': [
            'additionalA'
        ],
        'acknowledgements': 'some acknowledgement',
        'citationInstructions': [
            'citeMeA', 'citeMeB'
        ],
    }
    dataset['datasetID'] = pytest.TEST_DATASET_ID
    res = client.post('/upload_dataset',
        data=json.dumps(dataset),
        content_type='application/json',
        query_string={'token': token}
    )
    res_dict = json.loads(res.data)
    assert res_dict.get('success') is not None 

def test_request_tiles(client):
    '''
    test that we can request tiles by id and zoom level (z, y, x order)
    '''
    print(pytest.TEST_TILES_ID)
    res = client.get('/viewer_tiles/{}/{}/{}/{}'.format(
        pytest.TEST_TILES_ID, 0, 0, 0
        )
    )
    print('/viewer_tiles/{}/{}/{}/{}'.format(
        pytest.TEST_TILES_ID, 0, 0, 0
        ))
    print(res)
    assert res is not None

def test_query_dataset_by_cat(client):
    '''
    test that we can query datasets by category (should return a list of datasets)
    '''
    res = client.get('/datasets', query_string={'category':'zoo'})
    res_dict = json.loads(res.data)
    print(res_dict)
    assert res_dict.get('success') is not None 

def test_query_dataset_by_id(client):
    '''
    test that we can query a specific dataset by an id
    '''
    res = client.get('/datasets', query_string={'id':pytest.TEST_DATASET_ID})
    res_dict = json.loads(res.data)
    print(res_dict)
    assert res_dict.get('success') is not None 

