from datetime import datetime, timedelta
import os
from mongoengine import Document, ListField, StringField, connect, EmbeddedDocument, ReferenceField, EmbeddedDocumentField, BooleanField
from mongoengine.fields import DateTimeField

# add password and user from env var to connect
connect(
    db='dbb',
    host="mongodb://" + os.environ.get("DBB_DB_HOST"),
    port=27017,
    username=os.environ.get("DBB_ADMIN_USERNAME"),
    password=os.environ.get("DBB_ADMIN_PASSWORD"),
    authentication_source='admin'
)

class User(Document):
    '''
    A User is a person that wishes to download DBB data after signing an MTA
    '''
    username = StringField(required=True)
    name = StringField(required=True)
    email = StringField(required=True)
    passwordHash = StringField(required=True)
    authToken = StringField(default='')
    authTokenExpireDate = DateTimeField(default=datetime.utcnow())
    createdAt = DateTimeField(default=datetime.utcnow())


class DatasetContent(EmbeddedDocument):
    '''
    DatasetContent consists of a title and a list of images.
    The title is displayed as a subheading, and the list
    of images are displayed as a bullet list.
    '''
    title = StringField(default='')
    content = ListField(default=list)

class Publication(EmbeddedDocument):
    '''
    A Publication has a plain text citation and a link url
    '''
    citation = StringField(default='')
    url = StringField(default='')


class Contributor(Document):
    '''
    A Contributor has a string name and a string role
    '''
    username= StringField(required=True)
    name = StringField(required=True)
    biosketch = StringField(default='')
    role = StringField(required=True)
    email = StringField(required=True)
    passwordHash = StringField(required=True)
    authToken = StringField(default='')
    authTokenExpireDate = DateTimeField(default=datetime.utcnow())
    createdAt = DateTimeField(default=datetime.utcnow())
    datasets = ListField(ReferenceField('Dataset'))


class CitationInstruction(EmbeddedDocument):
    '''
    A CitationInstruction has string text for an instruction sentence and a string link url
    '''
    text = StringField(default='')
    url = StringField(default='')


class DownloadableFile(Document):
    '''
    A DownloadableFile has a given name and a uuid
    '''
    name = StringField(required=True)
    uid = StringField(required=True)


class ViewerTile(Document):
    '''
    A ViewerTile contains a given name and a uid folder name to the folder containing the tiles.
    You can also set a displayName and the boolean isBaseLayer
    '''
    uid = StringField(required=True)
    displayName = StringField(required=True)
    isBaseLayer = BooleanField(default=False)


class Info(EmbeddedDocument):
    '''
    Info contains information about the dataset. It contains embedded documents
    '''
    associatedPublications = ListField(StringField)#EmbeddedDocumentField(Publication))
    mainContributors = ListField(StringField)#ReferenceField(Contributor))
    additionalContributors = ListField(StringField)#ReferenceField(Contributor))
    acknowledgements = StringField(default='')
    citationInstructions = ListField(StringField)#EmbeddedDocumentField(CitationInstruction))

class PublicFile(Document):
    '''
    a Thumbnail image is displayed next to each dataset on the website. It is a public static asset
    '''
    name = StringField(required=True)
    uid = StringField(required=True)

    def to_dict(self):
        return {
            'name': self.name,
            'uid': self.uid,
            'id': str(self.id)
        }


class Audit(Document):
    '''
    track dataset changes
    '''
    pass


class Dataset(Document):
    '''
    Dataset contains all information about a single digital brain bank dataset.
    Datasets and their attributes are displayed on the digital brain bank website
    '''
    category = StringField(required=True) # valid options are: '', 'zoo', 'pathologist', 'anatomist'
    datasetTitle = StringField(required=True) # the title string for this dataset (can be anything)
    datasetContentList = ListField(EmbeddedDocumentField(DatasetContent), default=list) # a list of DatasetContent items
    datasetContentFooter = StringField(default='') # an informative string to display below dataset content
    thumbnail = ReferenceField(PublicFile, required=True)
    viewerTextDescription = StringField(default='', required=True)
    downloadableFiles = ListField(ReferenceField(DownloadableFile), default=list)
    previewFiles = ListField(ReferenceField(PublicFile), default=list)
    viewerTiles = ListField(ReferenceField(ViewerTile), default=list)
    viewerStaticImages = ListField(ReferenceField(PublicFile), default=list)
    requiresMTA = BooleanField(default=True)
    correspondingAuthorEmail = StringField(default='')
    projectTitle = StringField(required=True)
    projectDescription = StringField(required=True)
    adminContributors = ListField(ReferenceField(Contributor), required=True)
    allowedUsers = ListField(ReferenceField(User), default=list) # need cascade remove
    #info = EmbeddedDocumentField(Info, required=True)
    associatedPublications = ListField(default=list)#EmbeddedDocumentField(Publication))
    mainContributors = ListField(default=list)#ReferenceField(Contributor))
    additionalContributors = ListField(default=list)#ReferenceField(Contributor))
    acknowledgements = StringField(default='')
    citationInstructions = ListField(default=list)#EmbeddedDocumentField(CitationInstruction))
    createdAt = DateTimeField(default=datetime.utcnow())
    deleted = BooleanField(default=False) # soft delete 
    lastmodifiedAt = DateTimeField(default=datetime.utcnow())
    isVisible=BooleanField(default=True)

    def to_dict(self):
        '''
        return a JSON serializable dict representation of a dataset. dereferences all reference fields
        '''
        # print(self.to_mongo())
        return {
            'category': self.category,
            'datasetTitle': self.datasetTitle,
            'datasetContentList': [{'title': content.title, 'content': content.content} for content in self.datasetContentList],
            'datasetContentFooter': self.datasetContentFooter,
            'thumbnail': PublicFile.objects(pk=self.thumbnail.id).first().to_dict(), #str(PublicFile.objects(pk=self.thumbnail.id).first().uid),
            'thumbnailURL': '/DigitalBrainBank/public_file/' + str(PublicFile.objects(pk=self.thumbnail.id).first().uid),
            'viewerTextDescription': self.viewerTextDescription,
            'downloadableFiles': [
                {
                    'name': file.name,
                    'uid': file.uid,
                    'url': '/DigitalBrainBank/protected_file/' + file.uid
                } 
                for file in self.downloadableFiles
            ],
            'previewFiles': [
                {
                    'name': file.name,
                    'uid': file.uid,
                    'url': '/DigitalBrainBank/public_file/' + file.uid
                } 
                for file in self.previewFiles
            ],
            'viewerTiles': [
                {
                    'displayName': file.displayName,
                    'uid': file.uid,
                    'isBaseLayer': file.isBaseLayer,
                    'url': '/DigitalBrainBank/viewer_tiles/' + str(file.id)
                } 
                for file in self.viewerTiles
            ],
            # str(PublicFile.objects(pk=self.viewerStaticImage.id).first().uid)
            'viewerStaticImages': [
                {
                    'name': file.name,
                    'uid': file.uid,
                    'url': '/DigitalBrainBank/public_file/' + file.uid
                } 
                for file in self.viewerStaticImages
            ],
            'requiresMTA': self.requiresMTA,
            'correspondingAuthorEmail': self.correspondingAuthorEmail,
            'projectTitle': self.projectTitle,
            'projectDescription': self.projectDescription,
            'adminContributors': [
                {
                    'name': contributor.name,
                    'username': contributor.username,
                    'biosketch': contributor.biosketch,
                    'email': contributor.email,
                } 
                for contributor in self.adminContributors
            ], 
            'allowedUsers': [
                {
                    'name': getattr(user, 'name', None),
                    'username': getattr(user, 'username', None),
                    'email': getattr(user, 'email', None),
                } 
                for user in self.allowedUsers
            ], 
            'associatedPublications':self.associatedPublications,
            'mainContributors':  self.mainContributors,
            'additionalContributors': self.additionalContributors,
            'acknowledgements': self.acknowledgements,
            'citationInstructions': self.citationInstructions,
            'createdAt': self.createdAt,
            'deleted': self.deleted,
            'lastModifiedAt': self.lastmodifiedAt,
            'isVisible': self.isVisible,
            'id': str(self.id)
        }




