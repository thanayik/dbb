#!/bin/bash -e
# source /home/dbb/venv/bin/activate
# cd dbbserver
#export SCRIPT_NAME=/DigitalBrainBank # see: https://dlukes.github.io/flask-wsgi-url-prefix.html
gunicorn -b 0.0.0.0:5000 --worker-tmp-dir /dev/shm --log-level debug --worker-class=gevent --worker-connections=1000 --timeout 3600 --workers 5 dbbserver:app

